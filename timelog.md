# Timelog

* Extracting Drug Relations from Biomedical Text
* Lenka Vincenova
* 2371607v
* Jake Lever

## Guidance

* This file contains the time log for your project. It will be submitted along with your final dissertation.
* **YOU MUST KEEP THIS UP TO DATE AND UNDER VERSION CONTROL.**
* This timelog should be filled out honestly, regularly (daily) and accurately. It is for *your* benefit.
* Follow the structure provided, grouping time by weeks.  Quantise time to the half hour.

## Week 1

#### 5 Oct 2021

* *4 hours* Reading articles and papers
 
#### 6 Oct 2021

* *0.5 hours* meeting with supervisor

#### 7 Oct 2021

* *2 hours* repository setup + wiki notes
#### 13 Oct 2021
* *0.5h* supervisor's meeting

## Week 3

#### 19 Oct 2021
* *5 hours* loading datasets into python and creating a format that is suitable
* *5 hours* working on parse tree

#### 20 Oct 2021
* *0.5h* meeting

## Week 4

#### 26 Oct 2021
* *8h* correcting loading data, fixing bugs

#### 27 Oct 2021
* *0.5h* meeting

## Week 6


#### 7,8,9 Nov 2021
* *10h* understanding/ finding where is the bug
* *10h* making a small progress in removing the error

#### 10 Nov 2021
* *0.5h* supervisor's meeting

## Week 7

#### 15, 16 Nov 2021
* *16h* improving loading data

## Week 8

#### 23 Nov 2021
* *15h* improving loading data

#### 24 Nov 2021
* *0.5h* supervisor's meeting

## Week 11

#### 14 Dec 2021
* *10h* status report, reading about nlp tokenization and bert deep learning, project plan during christmas break

#### 15 Dec 2021
* *0.5h* supervisor's meeting



# Semester 2

## Week 1

#### 8, 9 Jan 2022
* *8h* trying nltk to load data


#### 10 Jan 2022
* *4h* looking into errors while loading data using nltk

#### 11 Jan 2022
* *4h* continue from yesterday

#### 14 Jan 2022
* *2h* making training dataset with different types of relations (words inbetween, dep. path, ...)

## Week 2

#### 17 Jan 2022
* *5h* adding sentences to each relation

#### 18 Jan 2022
* *4h* bert transformer

#### 19 Jan 2022
* *4h* classifying with bert embedded vectors
* *0.5h* supervisor's meeting

## Week 3
#### 24 Jan 2022
* *5h* bert classifier

#### 25 Jan 2022
* *8h* bert classifier with scikit models

#### 26 Jan 2022
* *0.5h* supervisor's meeting

## Week 4
#### 1 Feb 2022
* *2h* setting code and classifier for the experiments

#### 2 Feb 2022
* *0.5h* supervisor's meeting

## Week 7

#### 24 Feb 2022
* *8h* refactoring extract relations

#### 25 Feb 2022
* *0.5h* supervisor's meeting
* *5h* indexing bert tokens in order to utilize spacy tokens indexes

#### 26 Feb 2022
* *8h* experiment, transfering code to deepnote/google colab

#### 27 Feb 2022
* *10h* running code, testing correctness
#### 26 Feb 2022
* *8h* bert embeddings for relations, debugging non_relations
#### 27 Feb 2022
* *8h* bert embeddings for relations, and non-relations

## Week 8

#### 28 Feb 2022
* *10h* bert setting up kaggle environment for classification


#### 1 March 2022
* *10h* building models on kaggle
#### 2 March 2022
* *5h* building models on kaggle, experimenting with different parameters
#### 3 March 2022
* *0.5h* supervisor's meeting
* *8h* building models on kaggle, experimenting with different parameters


## Week 9
#### 7 March 2022
* *5h* classification experiments
#### 8 March 2022
* *8h* extraction validation data + classification experiments

#### 9 March 2022
* *0.5h* supervisor's meeting

## Week 10+

#### 15 March 2022
* *10h* found error in code - redoing data parsing

#### 16 MArch 2022
* *0.5h* supervisor's meeting

#### 17 March 2022
* *10h* redoing BERT data as the dataset got updated

#### 21 March 2022
* *10h* BERT embeddings did not save properly, had to redone them

#### 29-30 March 2022
* *20h* re-running classification models, making graphs, tables 

#### 28-31 March 2022
* *8h* implementation chapter
* *8h* evaluation chapter
* *8h* analysis chapter

#### 31 March 2022
* *0.5h* supervisor's meeting
#### 1 April 2022
* *8h* implementing feedback received from supervisor

#### 2-4 April 2022
* *8h* introduction + conclusion chapter
* *20h* background chapter

#### 5-7 April 2022
* *30h* improving background chapter

#### 7 April 2022
* *8h* implementing feedback on first draft

#### 8 April 2022
* *8h* finalizing submission material