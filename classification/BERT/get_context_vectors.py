from transformers import BertTokenizer, BertModel
import pandas as pd
from classification.BERT.bert_fn import *
from intervaltree import IntervalTree
from transformers import AutoTokenizer

pd.set_option('max_columns', None)

REL_FILENAME = "relations_set.csv" # "val_relations_set.csv"  # "relations_set_v2.csv"
NON_REL_FILENAME = "non_relations_set.csv" # "val_non_relations_set.csv"  # "relations_set_v2.csv"
DATA_PATH = "../../data/extracted_data/training_data" #"../../data/extracted_data/validation_data"  # "../../data/extracted_data/training_data"


def int_tree(dep_path, idx):
    tree = IntervalTree()
    for i in range(len(dep_path)):
        # print(dep_path[i])
        start = idx[i]
        end = start + len(dep_path[i])
        # print(row.sentence[start:end])
        tree[start:end] = True
    return tree


def path_indexes(tree, tokenized):
    dep_path_indices = []
    for i, (t_start, t_end) in enumerate(tokenized['offset_mapping']):
        if tree[t_start:t_end]:
            dep_path_indices.append(i)
    return np.array(dep_path_indices)


def str_to_list(x):
    return x.split("###")


def to_str(x):
    return " ".join(x)


def int_to_list(x):
    return x.split(",")


def to_int(arr):
    return list(map(int, arr))


cols = ['dep_path', 'category', 'sentence', 'path_idx']
relations = pd.read_csv(f"{DATA_PATH}/{REL_FILENAME}")[cols]
non_relations = pd.read_csv(f"{DATA_PATH}/{NON_REL_FILENAME}")[cols]
non_relations['category'].fillna('NONE', inplace=True)

relations['dep_path'] = relations['dep_path'].apply(str_to_list)  # .apply(to_str) # list of strings
relations['path_idx'] = relations['path_idx'].apply(int_to_list).apply(to_int)  # list of ints
non_relations['dep_path'] = non_relations['dep_path'].apply(str_to_list)  # .apply(to_str) # list of strings
non_relations['path_idx'] = non_relations['path_idx'].apply(int_to_list).apply(to_int)  # list of ints

model_name = 'microsoft/BiomedNLP-PubMedBERT-base-uncased-abstract'

tokenizer = AutoTokenizer.from_pretrained(model_name)
model = BertModel.from_pretrained(model_name,
                                  output_hidden_states=True,
                                  )


def main(data, filename):
    embedded_data = pd.DataFrame(columns=['sentence_embedding', 'dep_path_embedding', 'category'])
    for i, row in data.iterrows():
        print(f"\r{i}/{len(data)}", end="")
        try:
            tree = int_tree(row.dep_path, row.path_idx)
            tokenized, tokens_tensor, segments_tensors = bert_text_preparation(row.sentence, tokenizer)
            indices = path_indexes(tree, tokenized)
            # TODO check the tokenized tensors and cut them to be under 512
            if tokens_tensor.size()[1] > 512:
                tokens_tensor, segments_tensors = tokens_tensor[:,:512], segments_tensors[:,:512]
                indices = indices[indices < 512]

            list_token_embeddings = get_bert_embeddings(tokens_tensor, segments_tensors, model)

            # average embeddings into a single (768,) vecotr
            sent_embedding = average_vector(list_token_embeddings)
            path_embedding = average_vector(list_token_embeddings, indices)

            if np.any(np.isnan(path_embedding)): #ignore if the dep path happened after 512th token
                continue
            # save to dataframe
            embedded_data = embedded_data.append({'sentence_embedding': sent_embedding,
                                                  'dep_path_embedding': path_embedding,
                                                  'category': row.category}, ignore_index=True)
            # save to file in 500 rows junks
            if (i%1000)==0:
                ed = embedded_data.to_numpy()
                with open(f"data/{filename}.npy", 'wb') as f:
                    np.save(f, ed)
        except Exception as e:
            print(e)
            continue

    ed = embedded_data.to_numpy()
    with open(f"data/{filename}.npy", 'wb') as f:
        np.save(f, ed)


print("tokenizing...")
from datetime import datetime

# #VALIDATION DATA
# start = datetime.now()
# main(relations, "val_relations_embeddings")
# end = datetime.now()
# print()
# print("Duration ", end - start)
#
# start = datetime.now()
# # half2 = non_relations.iloc[63001:]
# main(non_relations, "val_non_relations_embeddings")
# end = datetime.now()
# print()
# print("Duration ", end - start)

# TRAINING DATA
start = datetime.now()
main(relations, "relations_embeddings")
end = datetime.now()
print()
print("Duration ", end - start)



# start = datetime.now()
# part1 = non_relations.iloc[:80000]
# part2 = non_relations.iloc[80000:]
# main(part1, "non_relations_embeddings_part1")
# main(part2, "non_relations_embeddings_part2")
# end = datetime.now()
# print()
# print("Duration ", end - start)

"""

10915/16285The expanded size of the tensor (542) must match the existing size (512) at non-singleton dimension 1.  Target sizes: [1, 542].  Tensor sizes: [1, 512]
10916/16285The expanded size of the tensor (542) must match the existing size (512) at non-singleton dimension 1.  Target sizes: [1, 542].  Tensor sizes: [1, 512]
10917/16285The expanded size of the tensor (542) must match the existing size (512) at non-singleton dimension 1.  Target sizes: [1, 542].  Tensor sizes: [1, 512]
10918/16285The expanded size of the tensor (542) must match the existing size (512) at non-singleton dimension 1.  Target sizes: [1, 542].  Tensor sizes: [1, 512]
10919/16285The expanded size of the tensor (542) must match the existing size (512) at non-singleton dimension 1.  Target sizes: [1, 542].  Tensor sizes: [1, 512]
10920/16285The expanded size of the tensor (542) must match the existing size (512) at non-singleton dimension 1.  Target sizes: [1, 542].  Tensor sizes: [1, 512]
10921/16285The expanded size of the tensor (542) must match the existing size (512) at non-singleton dimension 1.  Target sizes: [1, 542].  Tensor sizes: [1, 512]
10922/16285The expanded size of the tensor (542) must match the existing size (512) at non-singleton dimension 1.  Target sizes: [1, 542].  Tensor sizes: [1, 512]
10923/16285The expanded size of the tensor (542) must match the existing size (512) at non-singleton dimension 1.  Target sizes: [1, 542].  Tensor sizes: [1, 512]
10924/16285The expanded size of the tensor (542) must match the existing size (512) at non-singleton dimension 1.  Target sizes: [1, 542].  Tensor sizes: [1, 512]
10925/16285The expanded size of the tensor (542) must match the existing size (512) at non-singleton dimension 1.  Target sizes: [1, 542].  Tensor sizes: [1, 512]
10926/16285The expanded size of the tensor (542) must match the existing size (512) at non-singleton dimension 1.  Target sizes: [1, 542].  Tensor sizes: [1, 512]
10927/16285The expanded size of the tensor (542) must match the existing size (512) at non-singleton dimension 1.  Target sizes: [1, 542].  Tensor sizes: [1, 512]
10928/16285The expanded size of the tensor (542) must match the existing size (512) at non-singleton dimension 1.  Target sizes: [1, 542].  Tensor sizes: [1, 512]
0:54:12.313888

Process finished with exit code 0


troubled sentence for both relation & non-relation:
# In the present studies, the effects of CB(1) agonists [(6aR,10aR)-3-(1-adamantyl)-6,6,9-trimethyl-6a,7,10,
# 10a-tetrahydrobenzo[c]chromen-1-ol (AM411), 9β-(hydroxymethyl)-3-(1-adamantyl)-hexahydrocannabinol (AM4054), 
# R-(+)-[2,3-dihydro-5-methyl-3-[(morpholinyl)methyl]pyrrolo[1,2,3-de]-1,4-benzoxazinyl]-(1-naphthalenyl)methanone 
# mesylate (WIN55,212.2), Δ(9)-tetrahydrocannabinol (Δ(9)-THC), (R)-(+)-arachidonyl-1'-hydroxy-2'-propylamide (
# methanandamide)], CB(1) antagonists [5-(4-chlorophenyl)-1-(2,4-dichloro-phenyl)-4-methyl-N-(
# piperidin-1-yl)-1H-pyrazole-3-carboxamide (SR141716A), 5-(4-alkylphenyl)-1-(2,4-dichlorophenyl)-4-methyl-N-(
# piperidin-1-yl)-1H-pyrazole-3-carboxamide (AM4113)], and dopamine (DA)-related [methamphetamine, (±)-6-chloro-7,
# 8-dihydroxy-3-allyl-1-phenyl-2,3,4,5-tetrahydro-1H-3-benzazepine hydrobromide (SKF82958), 
# (R)-(+)-7-chloro-8-hydroxy-3-methyl-1-phenyl-2,3,4,5-tetrahydro-1H-3-benzazepine hydrochloride (SCH23390), (6aR)-5,
# 6,6a,7-tetrahydro-6-propyl-4H-dibenzo[de,g]quinoline-10,11-diol (R-(-)-NPA), haloperidol] and opioid (morphine, 
# naltrexone) drugs on scheduled-controlled responding under a 30-response fixed ratio schedule of stimulus-shock 
# termination in squirrel monkeys were compared before and during chronic treatment with the long-acting CB(1) 
# agonist AM411 (1.0 mg/kg per day, i.m.). 

"""
