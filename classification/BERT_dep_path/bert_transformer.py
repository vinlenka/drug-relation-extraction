from transformers import BertTokenizer, BertModel
import pandas as pd
import numpy as np
from classification.BERT.bert_fn import *
from intervaltree import IntervalTree

cols = ['dep_path', 'category', 'sentence', 'path_idx', 'arg1_idx', 'arg2_idx']
relations = pd.read_csv("../../data/spacy/relations_set_v2.csv")[cols].dropna(axis=0, how="any")
# non_relations = pd.read_csv("../../data/spacy/non_relations_set.csv")[['dep_path', 'category']]
# non_relations_smaller = non_relations.sample(n=len(relations))

size = 10
data = relations#.iloc[:size]  # .sample(n=size)#.append(non_relations_smaller.sample(n=size))


def str_to_list(x):
    return x.split("###")


def to_str(x):
    return " ".join(x)


def int_to_list(x):
    return x.split(",")


def to_int(arr):
    return list(map(int, arr))


data['dep_path'].fillna("", inplace=True)
data['path_idx'].fillna("", inplace=True)
data['dep_path'] = data['dep_path'].apply(str_to_list)  # .apply(to_str) # list of strings
data['path_idx'] = data['path_idx'].apply(int_to_list).apply(to_int)  # list of ints

data['sentence'] = data['sentence'].apply(str_to_list).apply(to_str)  # string

# resulting frame
embedded_data = pd.DataFrame(columns=['vector', 'category'])

# Loading the pre-trained BERT model
###################################
# Embeddings will be derived from
# the outputs of this model

# Setting up the tokenizer
###################################
# This is the same tokenizer that
# was used in the model to generate
# embeddings to ensure consistency
from transformers import AutoTokenizer

model_name = 'microsoft/BiomedNLP-PubMedBERT-base-uncased-abstract'

tokenizer = AutoTokenizer.from_pretrained(model_name)
# tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
model = BertModel.from_pretrained(model_name,
                                  output_hidden_states=True,
                                  )

# Getting embeddings for the target
# word in all given contexts
target_word_embeddings = []


def get_index(path, text):
    j = 0
    idx = []
    for i in range(0, len(text) - 1):
        if text[i + 1].startswith("##"):
            combined = text[0] + text[1][2:]


def int_tree(dep_path, idx):
    tree = IntervalTree()
    for i in range(len(dep_path)):
        # print(dep_path[i])

        start = idx[i]
        end = start + len(dep_path[i])
        # print(row.sentence[start:end])
        tree[start:end] = True
    return tree


def path_indexes(tree, tokenized):
    dep_path_indices = []
    # print(tree)
    for i, (t_start, t_end) in enumerate(tokenized['offset_mapping']):
        if tree[t_start:t_end]:
            dep_path_indices.append(i)

    return dep_path_indices


print("tokenizing...")
from datetime import datetime
start = datetime.now()
for i, row in data.iterrows():
    print(i)
    try:
        tree = int_tree(row.dep_path, row.path_idx)
        tokenized, tokens_tensor, segments_tensors = bert_text_preparation(row.sentence, tokenizer)
        indices = path_indexes(tree, tokenized)
        # text = [ tokenizer.decode(input_id) for input_id in tokenized['input_ids'] ]
        list_token_embeddings = get_bert_embeddings(tokens_tensor, segments_tensors, model)
        # average embeddings into a single (768,) vecotr
        sent_embedding = average_vector(list_token_embeddings, indices)
        # print(type(sent_embedding), sent_emrbedding.shape)
        embedded_data = embedded_data.append({'vector': sent_embedding, 'category': row.category}, ignore_index=True)

    except Exception as e:
        print(e)
        continue

print("FINISHED")
end = datetime.now()
print(end-start)
exit()
embedded_data.fillna('NONE', inplace=True)
ed = embedded_data.to_numpy()
with open("bert_data/embedded100.npy", 'wb') as f:
    np.save(f, ed)
