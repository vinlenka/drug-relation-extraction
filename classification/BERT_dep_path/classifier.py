from sklearn.neural_network import MLPClassifier
from sklearn.svm import LinearSVC
from sklearn import metrics
from sklearn.model_selection import train_test_split
import pandas as pd
import numpy as np
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline
from datetime import datetime
from sklearn.metrics import ConfusionMatrixDisplay
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt


prefix = "../BERT/data"
with open(f"{prefix}/non_relations_embeddings_part1.npy", 'rb') as f:
    embedded_data1 = np.load(f, allow_pickle=True)

with open(f"{prefix}/non_relations_embeddings_part2.npy", 'rb') as f:
    embedded_data2 = np.load(f, allow_pickle=True)
non_relations = np.append(embedded_data1, embedded_data2, axis=0)

with open(f"{prefix}/relations_embeddings.npy", 'rb') as f:
    relations = np.load(f, allow_pickle=True)

non_rel_size = len(relations)
idx = np.arange(0, len(non_relations))
sample_idx = np.random.choice(idx, size=non_rel_size, replace=False, )

# embedded_data = np.append(relations, non_relations, axis=0)
embedded_data = np.append(relations, non_relations[sample_idx], axis=0)
# calculate weight of each class
cat_name, count = np.unique(embedded_data[:, 2], return_counts=True)
w = len(embedded_data) / (len(cat_name) * count)

embedded_data = np.append(relations, non_relations[sample_idx], axis=0)
# print(w)

# weights as dictionary
weights = dict(zip(cat_name, w))
print(weights)
# dictionary linking number to a cat. name
classes = {}
for c in range(len(cat_name)):
    classes[cat_name[c]] = c
rev_classes = {value: key for (key, value) in classes.items()}


def show_confusion_matrix(predicted, true, filename=None, clf=None):
    cm = confusion_matrix(true, predicted, labels=clf.classes_)
    disp = ConfusionMatrixDisplay(confusion_matrix=cm,
                                  display_labels=clf.classes_)
    fig, ax = plt.subplots(figsize=(30, 10))

    disp.plot(ax=ax)
    plt.xticks(rotation=90)
    if filename is not None:
        plt.savefig(f"{filename}")
    plt.show()

    #   dep path vector               , category


X_train, X_test, y_train, y_test = train_test_split(np.vstack(embedded_data[:, 1]), embedded_data[:, 2],
                                                    test_size=0.33, random_state=42, shuffle=True)


X_train, X_val, y_train, y_val = train_test_split(X_train, y_train, test_size=0.33, random_state=42, shuffle=True)

# clf = LinearSVC(random_state=1, class_weight = weights)
# start = datetime.now()
# clf.fit(X_train, y_train)
# predicted = clf.predict(X_test)
# print(metrics.classification_report(y_test, predicted))
#
# print(f"Duration: {datetime.now()-start}")
# show_confusion_matrix(y_test, predicted, "dep_path_linSVC")
#
#

# clf = MLPClassifier(solver='lbfgs', alpha=1e-5, epsilon=1e-03,
#                     hidden_layer_sizes=(128, 64), random_state=42)
# start = datetime.now()
# clf.fit(X_train, y_train)
# predicted = clf.predict(X_test)
# print(metrics.classification_report(y_test, predicted))
#
# print(f"Duration: {datetime.now()-start}")
#
# show_confusion_matrix(y_test, predicted, "dep_path_nn", clf=clf)


# {'alpha': 1e-06, 'epsilon': 0.0001, 'hidden_layer_sizes': (128, 64)}
params = [{'alpha': [1e-5, 1e-6, 1e-4],
           'epsilon': [1e-02, 1e-03,1e-04],
           'hidden_layer_sizes':[(128, 64), (96, 64), (64,32)]}]

gs = GridSearchCV(MLPClassifier(max_iter=1000),
                      param_grid=params,
                      scoring='f1_macro',
                      cv=2)


gs.fit(X_val, y_val)
print(gs.best_params_)
print("classifying with best params")
predicted = gs.predict(X_test)
print(metrics.classification_report(y_test, predicted))

show_confusion_matrix(y_test, predicted, clf = gs)


"""
             ACTIVATOR       0.59      0.51      0.55       474
               AGONIST       0.68      0.59      0.63       199
     AGONIST-ACTIVATOR       0.50      0.40      0.44         5
     AGONIST-INHIBITOR       0.00      0.00      0.00         2
            ANTAGONIST       0.67      0.70      0.69       293
      DIRECT-REGULATOR       0.63      0.65      0.64       724
INDIRECT-DOWNREGULATOR       0.64      0.65      0.64       440
  INDIRECT-UPREGULATOR       0.61      0.63      0.62       436
             INHIBITOR       0.75      0.78      0.76      1750
                  NONE       0.83      0.81      0.82      5342
               PART-OF       0.70      0.70      0.70       232
            PRODUCT-OF       0.61      0.66      0.63       287
             SUBSTRATE       0.61      0.74      0.67       546
  SUBSTRATE_PRODUCT-OF       0.00      0.00      0.00         9

              accuracy                           0.75     10739
             macro avg       0.56      0.56      0.56     10739
          weighted avg       0.75      0.75      0.75     10739

"""