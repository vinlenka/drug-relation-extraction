# https://scikit-learn.org/stable/tutorial/text_analytics/working_with_text_data.html
# bag of words -> dependency path relation using scikit model

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import SGDClassifier
from sklearn import metrics
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split

import pandas as pd
import numpy as np
from matplotlib import pyplot as plt

data = pd.read_csv("../data/nltk/relations.csv").dropna(axis=0, how="any")[['dep_path', 'category']] # load dep_path + category and remove null values
# print(data.head)

dfg = data.groupby(['category']).count().sort_values(['dep_path'], ascending=False)
print(dfg)
dfg.plot.bar(figsize=(16, 22))
plt.show()


X_train, X_test, y_train, y_test = train_test_split(data.dep_path, data.category, test_size=0.1, random_state=42)


text_clf = Pipeline([
    ('vect', CountVectorizer()),
    ('tfidf', TfidfTransformer()),
    ('clf', SGDClassifier(loss='hinge', penalty='l2',
                          alpha=1e-3, random_state=42,
                          max_iter=5, tol=None)),
])

text_clf.fit(X_train, y_train)
predicted = text_clf.predict(X_test)
print(metrics.classification_report(y_test, predicted))
# print(metrics.confusion_matrix(y_test, predicted))

