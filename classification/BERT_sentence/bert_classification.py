from sklearn.neural_network import MLPClassifier
from sklearn import metrics
from sklearn.model_selection import train_test_split
import pandas as pd
import numpy as np

with open("bert_data/embedded100.npy", 'rb') as f:
    embedded_data = np.load(f, allow_pickle=True)


X_train, X_test, y_train, y_test = train_test_split(np.vstack(embedded_data[:,0]), embedded_data[:,1],
                                                    test_size=0.33, random_state=42, shuffle=True)

# MLPClassifier(hidden_layer_sizes=(100,), activation='relu', *, solver='adam', alpha=0.0001, batch_size='auto',
#               learning_rate='constant', learning_rate_init=0.001, power_t=0.5, max_iter=200, shuffle=True,
#               random_state=None, tol=0.0001, verbose=False, warm_start=False, momentum=0.9, nesterovs_momentum=True,
#               early_stopping=False, validation_fraction=0.1, beta_1=0.9, beta_2=0.999, epsilon=1e-08, n_iter_no_change=10,
#               max_fun=15000)

clf = MLPClassifier(solver='adam', alpha=1e-5, epsilon=1e-03,
                    hidden_layer_sizes=(128, 64), random_state=1)
clf.fit(X_train, y_train)
predicted = clf.predict(X_test)
print(metrics.classification_report(y_test, predicted))
# accuracy                      0.72
# macro avg      0.72   0.66    0.65
# weighted avg   0.72   0.72    0.72

from sklearn.svm import LinearSVC
clf = LinearSVC(random_state=1)
clf.fit(X_train, y_train)
predicted = clf.predict(X_test)
print(metrics.classification_report(y_test, predicted))

# accuracy                        0.70
# macro avg       0.66    0.63    0.64
# weighted avg    0.70    0.70    0.70
