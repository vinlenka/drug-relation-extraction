from transformers import BertTokenizer, BertModel
import pandas as pd
import numpy as np
from ..BERT.bert_fn import *
import torch

relations = pd.read_csv("../../data/spacy/relations_set.csv")[['sentence', 'category']].dropna(axis=0, how="any")
non_relations = pd.read_csv("../../data/spacy/non_relations_set.csv")[['sentence', 'category']]
non_relations_smaller = non_relations.sample(n=len(relations))

size = 100
data = relations.sample(n=size).append(non_relations_smaller.sample(n=size))
# print(len(data))


# resulting frame
embedded_data = pd.DataFrame(columns=['vector', 'category'])

# Loading the pre-trained BERT model
###################################
# Embeddings will be derived from
# the outputs of this model
model = BertModel.from_pretrained('bert-base-uncased',
                                  output_hidden_states=True,
                                  )

# Setting up the tokenizer
###################################
# This is the same tokenizer that
# was used in the model to generate
# embeddings to ensure consistency
tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')

# Getting embeddings for the target
# word in all given contexts
target_word_embeddings = []

print("tokenizing...")
for i, row in data.iterrows():
    print(i)
    try:
        tokenized_text, tokens_tensor, segments_tensors = bert_text_preparation(row.sentence, tokenizer)
        # embeddings for each token in a sentence
        list_token_embeddings = get_bert_embeddings(tokens_tensor, segments_tensors, model)
        # average embeddings into a single (768,) vecotr
        sent_embedding = average_vector(list_token_embeddings)
        embedded_data = embedded_data.append({'vector': sent_embedding, 'category': row.category}, ignore_index=True)

    except Exception as e:
        print(e)
        continue

# print(embedded_data)
embedded_data.fillna('NONE', inplace=True)
ed = embedded_data.to_numpy()
with open("bert_data/embedded100.npy", 'wb') as f:
    np.save(f, ed)
exit()





# Error indexes
#
# 11525
# The expanded size of the tensor (660) must match the existing size (512) at non-singleton dimension 1.  Target sizes: [1, 660].  Tensor sizes: [1, 512]
# 11526
# The expanded size of the tensor (660) must match the existing size (512) at non-singleton dimension 1.  Target sizes: [1, 660].  Tensor sizes: [1, 512]
# 11527
# The expanded size of the tensor (660) must match the existing size (512) at non-singleton dimension 1.  Target sizes: [1, 660].  Tensor sizes: [1, 512]
# 11528
# The expanded size of the tensor (660) must match the existing size (512) at non-singleton dimension 1.  Target sizes: [1, 660].  Tensor sizes: [1, 512]
# 11529
# The expanded size of the tensor (660) must match the existing size (512) at non-singleton dimension 1.  Target sizes: [1, 660].  Tensor sizes: [1, 512]
# 11530
# The expanded size of the tensor (660) must match the existing size (512) at non-singleton dimension 1.  Target sizes: [1, 660].  Tensor sizes: [1, 512]
# 11531
# The expanded size of the tensor (660) must match the existing size (512) at non-singleton dimension 1.  Target sizes: [1, 660].  Tensor sizes: [1, 512]
# 11532
# The expanded size of the tensor (660) must match the existing size (512) at non-singleton dimension 1.  Target sizes: [1, 660].  Tensor sizes: [1, 512]
# 11533
# The expanded size of the tensor (660) must match the existing size (512) at non-singleton dimension 1.  Target sizes: [1, 660].  Tensor sizes: [1, 512]
# 11534
# The expanded size of the tensor (660) must match the existing size (512) at non-singleton dimension 1.  Target sizes: [1, 660].  Tensor sizes: [1, 512]
# 11535
# The expanded size of the tensor (660) must match the existing size (512) at non-singleton dimension 1.  Target sizes: [1, 660].  Tensor sizes: [1, 512]
# 11536
# The expanded size of the tensor (660) must match the existing size (512) at non-singleton dimension 1.  Target sizes: [1, 660].  Tensor sizes: [1, 512]
# 11537
# The expanded size of the tensor (660) must match the existing size (512) at non-singleton dimension 1.  Target sizes: [1, 660].  Tensor sizes: [1, 512]
# 11538
# The expanded size of the tensor (660) must match the existing size (512) at non-singleton dimension 1.  Target sizes: [1, 660].  Tensor sizes: [1, 512]


embedded_data.to_csv("./bert_data/embedded.csv", index=False)
# exit()
# print(embedded_data)
# print(embedded_data.iloc[0].vector.shape)
