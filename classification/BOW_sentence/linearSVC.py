# general
import pandas as pd
import numpy as np
from datetime import datetime

# models
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer, TfidfTransformer
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from sklearn.pipeline import Pipeline
from sklearn.svm import LinearSVC
from sklearn.model_selection import GridSearchCV
import warnings

# metrics & display
from sklearn import metrics
from sklearn.metrics import ConfusionMatrixDisplay
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt

TEST_DATA_PATH = "../../data/extracted_data/validation_data"  # "../../data/extracted_data/training_data"
test_rel = "val_relations_set.csv"
test_non_rel = "val_non_relations_set.csv"

TRAIN_DIR = "../../data/extracted_data/training_data"
train_rel = "relations_set_v2.csv"
train_non_rel = "non_relations_set_v2.csv"

cols = ['sentence', 'category']
# TRAIN
train_relations = pd.read_csv(f"{TRAIN_DIR}/{train_rel}")[cols]
train_non_relations = pd.read_csv(f"{TRAIN_DIR}/{train_non_rel}")[cols]
train_non_relations['category'].fillna('NONE', inplace=True)

# TEST
test_relations = pd.read_csv(f"{TEST_DATA_PATH}/{test_rel}")[cols]
test_non_relations = pd.read_csv(f"{TEST_DATA_PATH}/{test_non_rel}")[cols]
test_non_relations['category'].fillna('NONE', inplace=True)

train_data = pd.concat([train_relations, train_non_relations], ignore_index=True)
test_data = pd.concat([test_relations, test_non_relations], ignore_index=True)

#   sentence vector               , category
X_train, X_val, y_train, y_val = train_test_split(train_data.sentence, train_data.category,
                                                  test_size=0.33, random_state=42, shuffle=True)
X_test, y_test = test_data.sentence, test_data.category,

# weights for each class
counts = y_train.value_counts()
weights = (len(y_train) / (len(counts) * counts)).to_dict()


def show_confusion_matrix(predicted, true, clf, title="", filename=None):
    cm = confusion_matrix(true, predicted, labels=clf.classes_)
    disp = ConfusionMatrixDisplay(confusion_matrix=cm,
                                  display_labels=clf.classes_)
    fig, ax = plt.subplots(figsize=(30, 10))

    disp.plot(ax=ax)
    plt.xticks(rotation=90)
    plt.title(title)
    if filename is not None:
        plt.savefig(f"{filename}")
    plt.show()


def show_confusion_matrix_normalize_color(predicted, true, clf, title="", filename=None):
    import matplotlib as mpl

    # Get the color map to use
    cmap = plt.get_cmap('viridis')

    # Resample it using a non-linear mapping
    power = 0.25  # You can adjust this to tweak the mapping
    colors = []
    for i in np.arange(0, 1, 0.01):
        colors.append(cmap(i ** power))

        # Create the new color map
    cmap = mpl.colors.LinearSegmentedColormap.from_list('mycolours', colors)

    fig, ax = plt.subplots(figsize=(40, 15))
    plt.title(title)
    # Pass it into the confusion matrix display function
    ConfusionMatrixDisplay.from_predictions(
        true, predicted, display_labels=clf.classes_, cmap=cmap, xticks_rotation='vertical', ax=ax)

    if filename is not None:
        plt.savefig(f"{filename}")
    plt.show()


def train_model(model, title=None, filename=None):
    clf = model  # LinearSVC(random_state=1)
    start = datetime.now()
    clf.fit(X_train, y_train)
    predicted = clf.predict(X_test)
    print(metrics.classification_report(y_test, predicted))

    print(f"Duration: {datetime.now() - start}")
    # show_confusion_matrix(predicted, y_test, clf=clf, title=title, filename=filename)
    show_confusion_matrix_normalize_color(predicted, y_test, clf, title=title, filename=filename+"norm")


# 1. Default setting
text_clf = Pipeline([('vect', CountVectorizer()),
                      ('tfidf', TfidfTransformer()),
                      ('clf', LinearSVC(random_state=1)),
 ])
train_model(text_clf, filename="BOW_sent_baseline")

# # 2. with weights
#
# text_clf = Pipeline([('vect', CountVectorizer()),
#                       ('tfidf', TfidfTransformer()),
#                       ('clf', LinearSVC(random_state=1, class_weight=weights)),
#  ])
# train_model(text_clf, filename="LSVC_S_weights")

# 3. grid search

# parameters = {'vect__token_pattern': [None, ""],
#               'vect__analyzer': ['word', 'char', ],
#               'tfidf__sublinear_tf': [True, False],
#               'clf__C': [0.5, 1.0, 1.1, 1.2],
#               'clf__loss': ['hinge', 'squared_hinge'],
#               'clf__class_weight': [None, weights]
#               }
#
# text_clf = Pipeline([('vect', CountVectorizer()),
#                      ('tfidf', TfidfTransformer()),
#                      ('clf', LinearSVC(random_state=1, max_iter=5000)),
#                      ])
#
# s = datetime.now()
# clf = GridSearchCV(text_clf, parameters, scoring='f1_macro', verbose=1, cv=2, n_jobs=2)
# clf.fit(X_val, y_val)
# print("BEST PARAMS:")
# print(clf.best_params_)
# print(f"Duration = {datetime.now() - s}")
#
# params = clf.best_params_
#
# # params={'clf__C': 1.0, 'clf__class_weight': {'NONE': 0.0798284743039786, 'INHIBITOR': 2.1489825282631037, 'DIRECT-REGULATOR': 5.387961245104102, 'SUBSTRATE': 5.636010781671159, 'ACTIVATOR': 8.143636080386353, 'INDIRECT-UPREGULATOR': 8.306689972985858, 'INDIRECT-DOWNREGULATOR': 8.554082801505482, 'ANTAGONIST': 12.571909571909572, 'PRODUCT-OF': 12.70019436345967, 'PART-OF': 15.429161747343565, 'AGONIST': 18.037957211870257, 'AGONIST-ACTIVATOR': 393.0375939849624, 'SUBSTRATE_PRODUCT-OF': 393.0375939849624, 'AGONIST-INHIBITOR': 933.4642857142857},
# # 'clf__loss': 'squared_hinge', 'tfidf__sublinear_tf': True, 'vect__analyzer': 'char', 'vect__token_pattern': None}
#
# text_clf = Pipeline([('vect', CountVectorizer(token_pattern=params['vect__token_pattern'], analyzer=params['vect__analyzer'])),
#                      ('tfidf', TfidfTransformer(sublinear_tf=params['tfidf__sublinear_tf'])),
#                      ('clf', LinearSVC(C=params['clf__C'], loss=params['clf__loss'],
#                                        class_weight=params['clf__class_weight'], random_state=1,
#                                        max_iter=5000)),
#
#                      ])
#
# train_model(text_clf, title="BoW vector representation\nbest parameters from gridsearch",
#             filename="graphs/LSVC_S_gridsearch")


##########################
"""
BEST PARAMS:
{'clf__C': 1.0, 'clf__class_weight': {'NONE': 0.0798284743039786, 'INHIBITOR': 2.1489825282631037, 'DIRECT-REGULATOR': 5.387961245104102, 'SUBSTRATE': 5.636010781671159, 'ACTIVATOR': 8.143636080386353, 'INDIRECT-UPREGULATOR': 8.306689972985858, 'INDIRECT-DOWNREGULATOR': 8.554082801505482, 'ANTAGONIST': 12.571909571909572, 'PRODUCT-OF': 12.70019436345967, 'PART-OF': 15.429161747343565, 'AGONIST': 18.037957211870257, 'AGONIST-ACTIVATOR': 393.0375939849624, 'SUBSTRATE_PRODUCT-OF': 393.0375939849624, 'AGONIST-INHIBITOR': 933.4642857142857}, 'clf__loss': 'squared_hinge', 'tfidf__sublinear_tf': True, 'vect__analyzer': 'char', 'vect__token_pattern': None}
Duration = 1:50:04.106348


"""