import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer, TfidfTransformer, HashingVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn import metrics
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from sklearn.pipeline import Pipeline

relations = pd.read_csv("../../data/nltk/relations.csv")[['sentence', 'category']].dropna(axis=0, how="any")
non_relations = pd.read_csv("../../data/nltk/non_relations.csv")[['sentence', 'category']]
non_relations_smaller = non_relations.sample(n=len(relations))

size = 100
data = relations.sample(n=size).append(non_relations_smaller.sample(n=size))
data.fillna("NONE", inplace=True)


X_train, X_test, y_train, y_test = train_test_split(data.sentence, data.category, test_size=0.1, random_state=42, shuffle=True)


from sklearn.svm import LinearSVC

text_clf = Pipeline([
    ('vect', CountVectorizer()),
    ('tfidf', TfidfTransformer()),
    ('clf', LinearSVC(random_state=1) )
    # MLPClassifier(solver='adam', alpha=1e-5, epsilon=1e-03,
                    # hidden_layer_sizes=(128, 64), random_state=1)),
])


text_clf.fit(X_train, y_train)
predicted = text_clf.predict(X_test)
print(metrics.classification_report(y_test, predicted))

