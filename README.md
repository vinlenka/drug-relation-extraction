# Extracting Drug Relations from Biomedical Text

University of Glasgow bachelor's project.

Following the recent interest of biomedical society in drug-gene interaction, we research options for extracting these interactions from abstracts of published biomedical papers. The ability to extract relations between biomedical entities can lead to drug discoveries, drug re-purposing, knowledge graph mining, drug response modeling, and many more. As it is becoming very challenging for scientists to keep up with ever-growing research papers publications, we believe there is a need for trends analysis tools which is discussed in this paper.

We researched two methods on how to represent a relation between a drug and a gene, and two methods on how to represent the text relation as a numerical vector. Our best-performing model was trained using the shortest dependency path between a drug and a gene vectorized with the PubMedBert transformer model. It scored an F1 score of 29\% in Track 1 - Text mining drug and chemical-protein interactions, BioCreative VII.

The code structure of the project is as follows:
* data folder
    * contains BioCreative VIII training and development datasets
    * contains extracted training and testing datasets that are used for classification 
* data_parse folder
  * contains code to parse relations and non-relations and saves it to data folder
* classification folder
    * BERT folder
        * code to vectorize the text relations with BERT model
    * classification_models folder
      * contains jupyter notebooks for classification
