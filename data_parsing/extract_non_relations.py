import spacy
import pandas as pd
import numpy as np
from itertools import combinations
from util_functions import load_data, shortest_path, add_error
from util_functions import LOAD_DIRS, SAVE_DIRS


# # LOAD DATA FROM TRAINING DATASET
# entities_col_names = ["PMID", "term_no", "type", "start", "end", "text"]
# entities = pd.read_csv("../data/training/drugprot_training_entities.tsv", sep="\t", names=entities_col_names)
#
# abstracts_col_names = ["PMID", "title", "abstract"]
# abstracts = pd.read_csv("../data/training/drugprot_training_abstracs.tsv", sep="\t", names=abstracts_col_names)
# abstracts['text_span'] = abstracts.apply(lambda row: row.title + " " + row.abstract, axis=1)
#
# relations_col_names = ["PMID", "relation", "arg1", "arg2"]
# relations = pd.read_csv("../data/training/drugprot_training_relations.tsv", sep="\t", names=relations_col_names)
# relations['arg1'] = relations.apply(lambda row: row.arg1[5:], axis=1)
# relations['arg2'] = relations.apply(lambda row: row.arg2[5:], axis=1)


def non_relations_main(data_dir, save_dir, validation_data=False, rel_filename="non_relations_set",
                       err_filename="non_rel_errors"):
    if validation_data:
        abstracts, entities, relations = load_data(path_prefix=data_dir, which_data="development")
    else:
        abstracts, entities, relations = load_data(path_prefix=data_dir)

    error_count = 0
    rel_errors = pd.DataFrame()
    non_relations_set = pd.DataFrame()

    nlp = spacy.load('en_core_web_sm')
    nlp.add_pipe('sentencizer')  # updated
    relations_count = 0
    for index, row in abstracts.iterrows():
        print(f"\r{index} / {len(abstracts)}", end='')
        ents = entities.loc[entities['PMID'] == row.PMID]
        rels = relations.loc[relations['PMID'] == row.PMID]
        doc = nlp(row.text_span)

        for sent in doc.sents:
            ents_in_sent = ents.loc[(ents['start'] >= sent.start_char) & (ents['end'] <= sent.end_char)]
            str_sent = sent.text

            if len(ents_in_sent) <= 1:
                continue
            pair_combinations = list(combinations(np.arange(len(ents_in_sent)), 2))
            for comb in pair_combinations:
                e1 = ents_in_sent.iloc[comb[0]]
                e2 = ents_in_sent.iloc[comb[1]]

                # skip if entities form a true relation
                if not (rels.loc[(rels['arg1'] == e1.term_no) & (rels['arg2'] == e2.term_no)]).empty:
                    continue

                # ARG 1
                arg1 = doc.char_span(int(e1.start), int(e1.end))  # spacy span of entity
                if arg1 is None:
                    error_count, rel_errors = add_error(row.PMID, error_count, rel_errors, "INDEXING", str_sent, e1, e2)
                    continue
                else:
                    start1, end1 = doc[arg1[0].i].i, doc[arg1[-1].i].i

                # ARG2
                arg2 = doc.char_span(int(e2.start), int(e2.end))  # spacy span of entity
                if arg2 is None:
                    error_count, rel_errors = add_error(row.PMID, error_count, rel_errors, "INDEXING", str_sent, e1, e2)
                    continue
                else:
                    start2, end2 = doc[arg2[0].i].i, doc[arg2[-1].i].i

                relation, length = shortest_path(sent, doc[end1], doc[end2], show_graphs=True)

                if relation is None:
                    error_count, rel_errors = add_error(row.PMID, error_count, rel_errors, "RELATION", sent.text, e1,
                                                        e2)
                    continue

                nonRel = {"PMID": row.PMID,
                          "dep_path": "###".join([str(tok.text).lower() for tok in relation]),
                          "length": length,
                          "category": None,
                          "arg1": str(e1.text).lower(),
                          'arg1_idx': ",".join([str(doc[end1].idx - sent.start_char),
                                                str((doc[end1].idx - sent.start_char) + len(doc[end1].text) - 1)]),
                          "arg2": str(e2.text).lower(),
                          'arg2_idx': ",".join([str(doc[end2].idx - sent.start_char),
                                                str((doc[end2].idx - sent.start_char) + len(doc[end2].text) - 1)]),
                          "type1": e1.type.lower(),
                          "type2": e2.type.lower(),
                          "sentence": sent.text.lower(),
                          "path_idx": ",".join([str(tok.idx - sent.start_char) for tok in relation])
                          }
                non_relations_set = non_relations_set.append(nonRel, ignore_index=True)

    print()
    print(f"error count = {error_count}")
    print(non_relations_set.count())

    non_relations_set.to_csv(path_or_buf=f"{save_dir}/{rel_filename}.csv", index=False)
    rel_errors.to_csv(path_or_buf=f"{save_dir}/errors/{err_filename}.csv", index=False)


from datetime import datetime

start = datetime.now()

non_relations_main(data_dir=LOAD_DIRS['TRAINING_DATA'],
                   save_dir=SAVE_DIRS['TRAINING_DIR'],
                   rel_filename="non_relations_set",
                   err_filename="non_rel_errors")

non_relations_main(data_dir=LOAD_DIRS['VALIDATION_DATA'],
                   save_dir=SAVE_DIRS['VALIDATION_DIR'],
                   validation_data=True,
                   rel_filename="val_non_relations_set",
                   err_filename="val_non_rel_errors")

print(f"\nDuration: {datetime.now() - start}")

# last run result
"""
error count = 10385
PMID        139758
dep_path    139758
length      139758
category         0
arg1        139719
arg1_idx    139758
arg2        139707
arg2_idx    139758
type1       139758
type2       139758
sentence    139758
path_idx    139758
dtype: int64

Duration: 5:47:54.440844


"""
