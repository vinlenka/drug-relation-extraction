import pandas as pd
import networkx as nx
from matplotlib import pyplot as plt

LOAD_DIRS = {
    "TRAINING_DATA": "../data/training",
    "VALIDATION_DATA": "../data/development"
}
SAVE_DIRS = {
    "TRAINING_DIR": "../data/extracted_data/training_data",
    "VALIDATION_DIR": "../data/extracted_data/validation_data",
}


# LOAD DATA FROM TRAINING DATASET
def load_data(path_prefix, which_data="training"):
    entities_col_names = ["PMID", "term_no", "type", "start", "end", "text"]
    entities = pd.read_csv(f"{path_prefix}/drugprot_{which_data}_entities.tsv", sep="\t", names=entities_col_names)

    abstracts_col_names = ["PMID", "title", "abstract"]
    abstracts = pd.read_csv(f"{path_prefix}/drugprot_{which_data}_abstracs.tsv", sep="\t", names=abstracts_col_names)
    abstracts['text_span'] = abstracts.apply(lambda row: row.title + " " + row.abstract, axis=1)

    relations_col_names = ["PMID", "relation", "arg1", "arg2"]
    relations = pd.read_csv(f"{path_prefix}/drugprot_{which_data}_relations.tsv", sep="\t", names=relations_col_names)
    relations['arg1'] = relations.apply(lambda row: row.arg1[5:], axis=1)
    relations['arg2'] = relations.apply(lambda row: row.arg2[5:], axis=1)

    return abstracts, entities, relations


def add_error(pmid, error_count, rel_errors, err_type, str_sent, e1, e2):
    error_count += 1
    rel_errors = rel_errors.append(
        # {"PMID": pmid, "ERROR": err_type, "sentence": str_sent, "arg1": e1.text.item(), "arg2": e2.text.item()},
        {"PMID": pmid, "ERROR": err_type, "sentence": str_sent, "arg1": e1.text, "arg2": e2.text},
        ignore_index=True)
    return error_count, rel_errors


def shortest_path(doc, arg1, arg2, show_graphs=True):
    edges = []
    try:
        for token in doc:
            for child in token.children:
                edges.append((token, child))
        graph = nx.Graph(edges)

        length = nx.shortest_path_length(graph, source=arg1, target=arg2)
        path = nx.shortest_path(graph, source=arg1, target=arg2)
        return path, length
    except:
        if show_graphs:
            nx.draw_networkx(graph)
            plt.title(" vs. ".join([str(arg1), str(arg2)]))
            plt.show()
        return None, None
