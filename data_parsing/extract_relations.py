import pandas as pd
import spacy

from util_functions import load_data, shortest_path, add_error
from util_functions import LOAD_DIRS, SAVE_DIRS


def relations_main(data_dir, save_dir, validation_data = False, rel_filename="relations_set", err_filename="rel_errors"):
    if validation_data:
        abstracts, entities, relations = load_data(path_prefix=data_dir, which_data="development")
    else:
        abstracts, entities, relations = load_data(path_prefix=data_dir)

    error_count = 0
    rel_count = 0
    rel_out_of_sent = 0
    rel_errors = pd.DataFrame()
    relations_set = pd.DataFrame()

    nlp = spacy.load('en_core_web_sm')
    # nlp.add_pipe('sentencizer')  # updated

    for index, row in abstracts.iterrows():
        print(f"\r{index} / {len(abstracts)}", end='')
        ents = entities.loc[entities['PMID'] == row.PMID]
        rels = relations.loc[relations['PMID'] == row.PMID]
        doc = nlp(row.text_span)
        for i, r in rels.iterrows():
            rel_count += 1
            e1 = ents.loc[ents['term_no'] == r.arg1]
            e2 = ents.loc[ents['term_no'] == r.arg2]

            for sent in doc.sents:
                str_sent = sent.text
                if int(e1.start) >= sent.start_char and int(e2.start) >= sent.start_char and int(
                        e1.end) <= sent.end_char and int(e2.end) <= sent.end_char:

                    arg1 = doc.char_span(int(e1.start), int(e1.end))  # spacy span of entity
                    if arg1 is None:
                        error_count, rel_errors = add_error(r.PMID, error_count, rel_errors, "INDEXING", str_sent, e1,
                                                            e2)
                        continue
                    else:
                        start1, end1 = doc[arg1[0].i].i, doc[arg1[-1].i].i

                    # ARG2
                    arg2 = doc.char_span(int(e2.start), int(e2.end))  # spacy span of entity
                    if arg2 is None:
                        error_count, rel_errors = add_error(r.PMID, error_count, rel_errors, "INDEXING", str_sent, e1,
                                                            e2)
                        continue
                    else:
                        start2, end2 = doc[arg2[0].i].i, doc[arg2[-1].i].i

                    relation, length = shortest_path(sent, doc[end1], doc[end2])

                    if relation is None:
                        error_count, rel_errors = add_error(r.PMID, error_count, rel_errors, "RELATION", sent.text, e1,
                                                            e2)
                        break

                    newRel = {"PMID": row.PMID,
                              "dep_path": "###".join([str(tok.text).lower() for tok in relation]),
                              "length": length,
                              "category": r.relation.lower(),
                              "arg1": str(e1.text.item()).lower(),
                              'arg1_idx': ",".join([str(doc[end1].idx - sent.start_char),
                                                    str((doc[end1].idx - sent.start_char) + len(doc[end1].text) - 1)]),

                              "arg2": str(e2.text.item()).lower(),
                              'arg2_idx': ",".join([str(doc[end2].idx - sent.start_char),
                                                    str((doc[end2].idx - sent.start_char) + len(doc[end2].text) - 1)]),
                              "type1": e1.type.item().lower(),
                              "type2": e2.type.item().lower(),
                              "sentence": sent.text.lower(),
                              "path_idx": ",".join([str(tok.idx - sent.start_char) for tok in relation])
                              }

                    relations_set = relations_set.append(newRel, ignore_index=True)
                    break
                else:
                    rel_out_of_sent += 1

    print()

    print(f"relations that are out of sentence = {len(relations['PMID']) - error_count - len(relations_set['PMID'])}")
    print(f"error count = {error_count}")
    print(f"no. of relations: {relations_set.count()}/ {len(relations)}")

    relations_set.to_csv(path_or_buf=f"{save_dir}/{rel_filename}.csv", index=False)
    rel_errors.to_csv(path_or_buf=f"{save_dir}/errors/{err_filename}.csv", index=False)


from datetime import datetime

start = datetime.now()

relations_main(data_dir=LOAD_DIRS['TRAINING_DATA'],
               save_dir=SAVE_DIRS['TRAINING_DIR'],
               rel_filename="relations_set3",
               err_filename="rel_errors3")

relations_main(data_dir=LOAD_DIRS['VALIDATION_DATA'],
               save_dir=SAVE_DIRS['VALIDATION_DIR'],
               validation_data=True,
               rel_filename="val_relations_set3",
               err_filename="val_rel_errors3")
print(f"\nDuration: {datetime.now() - start}")

"""
relations that are out of sentence = 241
error count = 748
no. of relations: PMID        16285
dep_path    16285
length      16285
category    16285
arg1        16281
arg1_idx    16285
arg2        16285
arg2_idx    16285
type1       16285
type2       16285
sentence    16285
path_idx    16285
dtype: int64/ 17274

Duration: 0:05:16.957664

Process finished with exit code 0
"""
